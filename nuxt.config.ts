import vuetify from 'vite-plugin-vuetify'

export default defineNuxtConfig({
  dir: {
    public: 'static',
  },
  app: {
    head: {
      title: process.env.npm_package_name || 'OpenZMS',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        {
          hid: 'description',
          name: 'description',
          content: process.env.npm_package_description || ''
        },
				{
          property: 'og:url',
          content: 'https://rdz.powderwireless.net/'
        },
        { property: 'og:type', content: 'website' },
        {
          property: 'og:title',
          content: 'POWDER-RDZ - POWDER Radio Dynamic Zone - OpenZMS'
        },
        {
          property: 'og:description',
          content: 'The POWDER-RDZ project investigates ways to share the electromagnetic (radio-frequency) spectrum between experimental or test systems and existing spectrum users, and between multiple experimental systems. This research team is deploying a prototype automatic spectrum sharing management system for the POWDER testbed in Salt Lake City, Utah (part of the NSF "Platforms for Advanced Wireless Research" program). Spectrum access challenges currently create significant constraints on experimentation and testing at wireless testbeds. Automatic spectrum sharing for safe access to additional frequencies beyond the frequencies reserved exclusively for testing will relax these constraints and thus increase the nation\'s capacity to conduct wireless research and development. Increasing this capacity will help accelerate growth and global leadership of the US communications industry, strengthen academic research into wireless systems, and benefit other spectrum-dependent sectors such as radar, public safety, and national defense. As a pathfinder for the National Radio Dynamic Zone concept, the project will help future federal/non-federal spectrum sharing arrangements assure that spectrum sharing does not negatively impact government users.  The POWDER-RDZ team is developing and deploying an end-to-end radio dynamic zone (RDZ) for advanced wireless communication. They will validate its functionality by performing spectrum sharing experiments and field studies on the resulting artifact. The project uses the existing POWDER mobile and wireless testbed as the physical infrastructure of the RDZ. POWDER\'s existing radios and other equipment supports the spectrum sharing experiments and provides part of the RF sensing functionality needed by the RDZ. The project is developing a modular zone management system (ZMS) to manage, control and monitor all aspects of the RDZ. The project plans to conduct experiments on spectrum sharing with users outside of POWDER. Experiments potentially include RDZ shared access to federal, non-federal, and commercial spectrum, such as coarse- and fine-grained spectrum sharing with a commercial mobile operator and spectrum sharing with a weather radar.'
        },
        {
          property: 'og:image',
          content: 'https://rdz.powderwireless.net/images/powderlogo-simple.svg'
        },
        { property: 'og:image:type', content: 'image/svg' },
        { property: 'og:image:alt', content: 'POWDER-RDZ Logo' }
			],
      link: [
        {
          rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'
        }
      ],
		},
	},
  devtools: {
    enabled: false
  },
  css: [
    'vuetify/lib/styles/main.sass',
    '@mdi/font/css/materialdesignicons.min.css'
  ],
  publicRuntimeConfig: {},
  privateRuntimeConfig: {
    serviceApiToken: process.env.SERVICE_API_TOKEN || 'undefined',
    gitHubClientID: process.env.GITHUB_CLIENT_ID || 'undefined',
    gitHubClientSecret: process.env.GITHUB_CLIENT_SECRET || 'undefined',
    googleClientID: process.env.GOOGLE_CLIENT_ID || 'undefined',
    googleClientSecret: process.env.GOOGLE_CLIENT_SECRET || 'undefined',
    cilogonClientID: process.env.CILOGON_CLIENT_ID || 'undefined',
    cilogonClientSecret: process.env.CILOGON_CLIENT_SECRET || 'undefined',
    origin: process.env.ORIGIN || 'undefined'
  },
  build: {
    transpile: ['vuetify'],
  },
  hooks: {
    'vite:extendConfig': (config) => {
      config.plugins.push(
        vuetify({
	        autoImport: true,
          styles: {
            configFile: './assets/settings.scss'
          },
        })
      )
    },
  },
  router: {
    mode: 'history'
  },
  modules: [
    '@sidebase/nuxt-auth',
    '@pinia/nuxt',
    '@pinia-plugin-persistedstate/nuxt',
    'nuxt-proxy'
  ],
  proxy: {
    options: [
      {
        pathFilter: '/zms/identity/',
        target: process.env.IDENTITY_URL
          ? process.env.IDENTITY_URL
          : 'http://localhost:8000/v1',
        changeOrigin: true,
        pathRewrite: { '^/zms/identity/': '/' }
      },
      {
        pathFilter: '/zms/zmc/',
        target: process.env.ZMC_URL
          ? process.env.ZMC_URL
          : 'http://localhost:8010/v1',
        changeOrigin: true,
        pathRewrite: { '^/zms/zmc/': '/' }
      },
      {
        pathFilter: '/zms/dst/',
        target: process.env.DST_URL
          ? process.env.DST_URL
          : 'http://localhost:8020/v1',
        changeOrigin: true,
        pathRewrite: { '^/zms/dst/': '/' }
      }
    ]
  },
  auth: {
    isEnabled: true,
    origin: process.env.ORIGIN || 'http://localhost:3000',
    baseURL: (process.env.ORIGIN || 'http://localhost:3000') + "/api/auth",
  }
})
