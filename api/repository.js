// Provides a repo factory plugin with handler callbacks

import * as qs from 'qs'

export default $fetch => (resource) => ({
  list(params, handler = defaultHandler) {
    let r = $fetch(`/${resource}`, {
      method: 'GET',
      params: params
    })
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  create(data, handler = defaultHandler) {
    let r = $fetch(`/${resource}`, {
      method: 'POST',
      body: jsonSerializer(data)
    })
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  read(id, params, handler = defaultHandler) {
    let r = $fetch(`/${resource}/${Array.isArray(id) ? id.join('/') : id}`, {
      method: 'GET',
      params: params
    })
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  readSecondary(id, secondaryResource, params, handler = defaultHandler) {
    let r = $fetch(`/${resource}/${Array.isArray(id) ? id.join('/') : id}/${secondaryResource}`, {
      method: 'GET',
      params: params
    })
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  update(id, data, handler = defaultHandler) {
    let r = $fetch(`/${resource}/${id}`, {
      method: 'POST',
      body: jsonSerializer(data)
    })
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  },
  delete(id, handler = defaultHandler) {
    console.log("delete url", `/${resource}/${id}`)
    let r = $fetch(`/${resource}/${id}`, {
      method: 'DELETE'
    })
    if (handler !== undefined && handler !== null) {
      return r.catch(handler)
    }
    return r
  }
})

function defaultHandler(e) {
  if (e.response) {
    console.log(e.response)
  }
  else if (e.request) {
    console.log(e.request)
  }
  else {
    console.log('Error', e.message, e)
  }
  //
  // There seems to be some lingering oddities where errors do not always
  // get bumped to the root vue error handler (and thus do not hit our
  // error.vue page).  Forcing uncaught errors to the error page is "always"
  // the right thing to do, so we make it happen.  The caller can override
  // defaultHandler, so, good enough.
  //
  // References:
  //   - https://github.com/nuxt/nuxt/issues/15432
  //   - https://nuxt.com/docs/getting-started/error-handling
  //
  showError(e)
  throw createError(e)
}

function jsonSerializer(data) {
  return JSON.stringify(data)
}

function queryStringSerializer(params) {
  return qs.stringify(params, { arrayFormat: 'repeat' })
}
