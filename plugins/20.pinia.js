import { userStore } from '~/stores/user'
import { clientStore } from '~/stores/client'
import { zoneStore } from '~/stores/zone'
import { adminStore } from '~/stores/admin'

export default defineNuxtPlugin(({ $pinia }) => {
  return {
    provide: {
      userStore: userStore($pinia),
      clientStore: clientStore($pinia),
      zoneStore: zoneStore($pinia),
      adminStore: adminStore($pinia)
    }
  }
})
