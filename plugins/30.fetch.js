export default defineNuxtPlugin(({ $userStore, $clientStore }) => {
  return {
    provide: {
      fetch: async (resource, options) => {
        if (false && !$userStore.token && resource.startsWith('/zms/identity/tokens')) {
          const headers = new Headers()
          headers.append('X-Api-Token', process.env.SERVICE_API_TOKEN)
          headers.append('X-Api-Elaborate', 'true')
          options = { ...options, headers }
        }
        else if ($clientStore.is_admin && $userStore.admin_token && resource.startsWith('/zms/')) {
          const headers = new Headers()
          headers.append('X-Api-Token', $userStore.admin_token.token)
          headers.append('X-Api-Elaborate', 'true')
          options = { ...options, headers }
        }
        else if ($userStore.token && resource.startsWith('/zms/')) {
          const headers = new Headers()
          headers.append('X-Api-Token', $userStore.token.token)
          headers.append('X-Api-Elaborate', 'true')
          options = { ...options, headers }
        }
        return $fetch(resource, options)
      }
    }
  }
})
