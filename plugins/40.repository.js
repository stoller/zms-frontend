import createRepository from '~/api/repository'

export default defineNuxtPlugin(({ $fetch }) => {
  const zmsApiFactory = createRepository($fetch)

  return {
    provide: {
      tokensEndpoint: zmsApiFactory('zms/identity/tokens'),
      usersEndpoint: zmsApiFactory('zms/identity/users'),
      elementsEndpoint: zmsApiFactory('zms/identity/elements'),
      servicesEndpoint: zmsApiFactory('zms/identity/services'),
      rolesEndpoint: zmsApiFactory('zms/identity/roles'),
      spectrumEndpoint: zmsApiFactory('zms/zmc/spectrum'),
      locationsEndpoint: zmsApiFactory('zms/zmc/locations'),
      radiosEndpoint: zmsApiFactory('zms/zmc/radios'),
      antennasEndpoint: zmsApiFactory('zms/zmc/antennas'),
      grantsEndpoint: zmsApiFactory('zms/zmc/grants'),
      claimsEndpoint: zmsApiFactory('zms/zmc/claims'),
      monitorsEndpoint: zmsApiFactory('zms/zmc/monitors'),
      observationsEndpoint: zmsApiFactory('zms/dst/observations')
    }
  }
})
