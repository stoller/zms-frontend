import { useNuxtApp } from '#app'
import { NuxtAuthHandler, getServerSession } from '#auth'
import GithubProvider from 'next-auth/providers/github'
import GoogleProvider from 'next-auth/providers/google'
import CredentialsProvider from 'next-auth/providers/credentials'

export default NuxtAuthHandler({
  secret: process.env.SERVICE_API_KEY,
  events: {
    signIn: (event) => {
      //console.log("signIn", event)
    }
  },
  callbacks: {
    async jwt({ token, user, account, profile, isNewUser, trigger }) {
      //console.log("jwt callback", token, user, account, profile)
      // Persist the OAuth access_token and or the user id to the token right after signin
      if (account) {
        token.access_token = account.access_token
        token.provider = account.provider
        if (user !== 'undefined') {
          token.token = user
        }
      }
      return token
    },
    async session({session, token, user}) {
      //console.log("session callback", session, token, user)
      if (token) {
        session.provider = token.provider
        session.access_token = token.access_token
        if (token.token !== 'undefined') {
          session.token = token.token
        }
      }
      return session
    },
  },
  providers: [
    GithubProvider.default({
      clientId: process.env.GITHUB_CLIENT_ID,
      clientSecret: process.env.GITHUB_CLIENT_SECRET,
    }),
    GoogleProvider.default({
      authorization: 'https://accounts.google.com/o/oauth2/auth',
      userInfo: 'https://www.googleapis.com/oauth2/v3/userinfo',
      token: 'https://oauth2.googleapis.com/token',
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET
    }),
    {
      id: 'cilogon',
      name: 'CILogon',
      type: 'oauth',
      version: '2.0',
      authorization: 'https://cilogon.org/authorize/',
      token: 'https://cilogon.org/oauth2/token',
      userinfo: 'https://cilogon.org/oauth2/userinfo',
      profile: (profile) => {
        return {
          id: profile.iss + profile.sub,
          name: profile.name,
          email: profile.email,
          affiliation: profile.affiliation
        }
      }
    },
    CredentialsProvider.default({
      id: 'credentials',
      name: 'OpenZMS Credentials',
      credentials: {
        username: { label: "Username", type: "text", placeholder: "Username" },
        password: { label: "Password", type: "password" }
      },
      async authorize(credentials: any) {
        console.log("credentials", credentials)
        console.log("endpoint", process.env.ORIGIN + '/zms/identity/tokens')
        const response = await fetch(process.env.ORIGIN + '/zms/identity/tokens', {
          method: 'POST',
          body: JSON.stringify({
            method: 'password',
            credential: {
              username: credentials.username,
              password: credentials.password,
            },
            admin_if_bound: true
          }),
          headers: {
            'Content-type': 'application/json',
            'X-Api-Elaborate': true
          }
        })
        console.log("credentials response", response)
        const token = await response.json()
        if (response.ok) {
          console.log("credentials token", token)
          return token
        } else {
          console.log("failed credentials login", response)
          return null
        }
      },
    }),
  ]
})
