

// Whacky problem importing the standard d3 top level module.
import * as d3 from './d3.js';
import * as _ from 'underscore';
import {computePosition, flip, shift,
	offset, autoPlacement} from '@floating-ui/dom';

function createFrequencyGraph(proxy) {
    var observation = proxy.observation;
    console.info("createFrequencyGraph", proxy);
    var data   = d3.csvParse(atob(observation.data), type);
    var bins   = CreateBins(data);
    
    createGraph("main", proxy, d3.select(".frequency-graph-maingraph"), bins);
}

function createGraph(which, proxy, graph, data)
{
    var container    = graph.select(".svg-container");
    var parent       = container.node().parentElement;
    var parentWidth  = parent.clientWidth;
    var parentHeight = parent.clientHeight;
    var ParentTop    = parent.offsetTop;
    var ParentLeft   = parent.offsetLeft;

    console.info("createGraph", data);
    console.info(container.node().clientWidth, container.node().clientHeight);
    console.info(parentWidth, parentHeight, ParentTop, ParentLeft);

    var margin  = {top: 20, right: 20, bottom: 130, left: 55};
    var width   = parentWidth - margin.left - margin.right;
    var height  = parentHeight - margin.top - margin.bottom;
    var margin2 = {top: parentHeight - 80,
		     right: 20, bottom: 30, left: 55};
    var height2 = parentHeight - margin2.top - margin2.bottom;
    console.info(width, height, height2);

    var bisector = d3.bisector(function(d) { return d.frequency; }).left;
    var formatter = d3.format(".3f");

    var x = d3.scaleLinear().range([0, width]),
	x2 = d3.scaleLinear().range([0, width]),
	y = d3.scaleLinear().range([height, 0]),
	y2 = d3.scaleLinear().range([height2, 0]);

    var xAxis = d3.axisBottom(x),
	xAxis2 = d3.axisBottom(x2),
	yAxis = d3.axisLeft(y);

    var brush = d3.brushX()
	.extent([[0, 0], [width, height2]])
	.on("brush end", brushed);

    var zoom = d3.zoom()
	.scaleExtent([1, Infinity])
	.translateExtent([[0, 0], [width, height]])
	.extent([[0, 0], [width, height]])
	.on("zoom", zoomed);

    var line = d3.line().curve(d3.curveStep)
        .x(function (d) { return x(d.frequency); })
        .y(function (d) { return y(which == "main" ? d.max : d.power); });

    var line2 = d3.line().curve(d3.curveStep)
        .x(function (d) { return x2(d.frequency); })
        .y(function (d) { return y2(which == "main" ? d.max : d.power); });

    var svg = container
	.append('svg')
        .attr("width", container.node().clientWidth)
        .attr("height", container.node().clientHeight);
    
    var clip = svg.append("defs").append("svg:clipPath")
        .attr("id", "clip")
        .append("svg:rect")
        .attr("width", width)
        .attr("height", height)
        .attr("x", 0)
        .attr("y", 0); 

    var Line_chart = svg.append("g")
        .attr("class", "focus")
        .attr("transform",
	      "translate(" + margin.left + "," + margin.top + ")")
	.attr("clip-path", "url(#clip)");

    var focus = svg.append("g")
        .attr("class", "focus")
        .attr("transform",
	      "translate(" + margin.left + "," + margin.top + ")");

    var context = svg.append("g")
	.attr("class", "context")
	.attr("transform",
	      "translate(" + margin2.left + "," + margin2.top + ")");

    x.domain(d3.extent(data, function(d) { return d.frequency; }));
    if (which == "main") {
	y.domain(d3.extent(data, function(d) { return d.max + 1; }));
    }
    else {
	// I want a little more pad above and below on the subgraph
	var power_extents = d3.extent(data, function(d) { return d.power; });
	power_extents[0] = power_extents[0] - 2;
	power_extents[1] = power_extents[1] + 2;
	console.info("power extents", power_extents);
	y.domain(power_extents);
    }
    x2.domain(x.domain());
    y2.domain(y.domain());

    focus.append("g")
	.attr("class", "axis axis--x")
	.attr("transform", "translate(0," + height + ")")
	.call(xAxis);

    // text label for the x axis
    focus.append("text")             
	.attr("y", height + margin.top + 15)
	.attr("x", (width / 2))
	.style("text-anchor", "middle")
	.text("Frequency (MHz)");

    focus.append("g")
	.attr("class", "axis axis--y")
	.call(yAxis);

    // text label for the y axis
    focus.append("text")
	.attr("transform", "rotate(-90)")
	.attr("y", 0 - margin.left)
	.attr("x",0 - (height / 2))
	.attr("dy", "1em")
	.style("text-anchor", "middle")
	.text("Power (dB)");      
    
    Line_chart.append("path")
	.datum(data)
	.attr("class", "line line-power")
	.attr("d", line);

    var tooltip = Line_chart.append("g")
	.attr("class", "tooltip")
	.style("opacity", "1.0")
	.style("display", "none");

    tooltip.append("circle")
	.attr("r", 5);

    context.append("path")
	.datum(data)
	.attr("class", "line")
	.attr("d", line2);

    context.append("g")
	.attr("class", "axis axis--x")
	.attr("transform", "translate(0," + height2 + ")")
	.call(xAxis2);

    context.append("g")
	.attr("class", "brush")
	.call(brush)
	.call(brush.move, x.range());

    var zoomer = svg.append("rect")
	.attr("class", "zoom")
	.attr("width", width)
	.attr("height", height)
	.attr("transform",
	      "translate(" + margin.left + "," + margin.top + ")")
	.call(zoom)
	.on("mouseover", ShowTooltip)
	.on("mouseout", HideTooltip)
	.on("mousemove", mousemove);

    function HideTooltip()
    {
	// The circle
	tooltip.style("display", "none");
	// The box
	graph.select(".popover-container").style("display", "none");
    }

    function ShowTooltip()
    {
	// The circle.
	tooltip.style("display", null);
	// The box
	graph.select(".popover-container").style("display", null);
    }

    function mousemove(event) {
	//console.info(event, d3.pointer(event));
	
	var x0 = x.invert(d3.pointer(event)[0]),
	    i = bisector(data, x0, 1),
	    d0 = data[i - 1],
	    d1 = data[i];

	var d = x0 - d0.frequency > d1.frequency - x0 ? d1 : d0;
	var tipX = x(d.frequency);
	var tipY;
	if (which == "main") {
	    tipY = y(d.max);
	}
	else {
	    tipY = y(d.power);
	}
	if (which == "main") {
	    proxy.mainToolTip.frequency = formatter(d.frequency);
	    proxy.mainToolTip.average   = formatter(d.avg);
	    proxy.mainToolTip.max       = formatter(d.max);
	    proxy.mainToolTip.min       = formatter(d.min);
	}
	else {
	    proxy.subToolTip.frequency  = formatter(d.frequency);
	    proxy.subToolTip.power      = formatter(d.power);
	    proxy.subToolTip.abovefloor = formatter(d.abovefloor);
	    proxy.subToolTip.violation  = d.violation;
	}
	
	//console.info(x0, d, x(d.frequency));
	//console.info(x(d.frequency), y(d.avg));

	// Move the circle to the correct location.
	tooltip.attr("transform", "translate(" + tipX + "," + tipY + ")");

	var ptop    = Math.floor(ParentTop + tipY + margin.top);
	var pleft   = Math.floor(ParentLeft + x(d.frequency)) + margin.left;
	// And compensate for scroll.
	ptop -= window.scrollY;

	// Need the popover and zoom elements for this.
	var popover  = graph.select(".popover-container").node();
	var zoomrect = zoomer.node().getBoundingClientRect();
	//console.info(zoomer.node());

	const virtualEl = {
	    getBoundingClientRect() {
		return {
		    width: 10,
		    height: 10,
		    left: pleft - 5,
		    top: ptop - 5,
		    right: pleft + 5,
		    bottom: ptop + 5,
		};
	    },
	};
	//console.info(virtualEl);
	
	// Yikes. https://floating-ui.com/docs/autoPlacement
	computePosition(virtualEl, popover, {
	    middleware: [
		autoPlacement({
		    allowedPlacements: ['left', 'right'],
		    boundry: zoomer.node(),
		}),
		offset({ mainAxis: 50 })
	    ],
	}).then(({x, y, placement, middlewareData}) => {
	    //console.log(x, y, placement, middlewareData); // any side
	    Object.assign(popover.style, {
		left: `${x}px`,
		top: `${y}px`,
	    });
	});
    }

    function brushed(event) {
	//console.info("brushed", event);
	if (!event.sourceEvent || event.sourceEvent.type === "zoom")
	    return; // ignore brush-by-zoom
	var s = event.selection || x2.range();
	x.domain(s.map(x2.invert, x2));
	Line_chart.select(".line-power").attr("d", line);
	focus.select(".axis--x").call(xAxis);
	svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
				 .scale(width / (s[1] - s[0]))
				 .translate(-s[0], 0));
    }

    function zoomed(event) {
	//console.info("zoomed", event);
	if (event.sourceEvent && event.sourceEvent.type === "brush")
	    return; // ignore zoom-by-brush
	var t = event.transform;
	x.domain(t.rescaleX(x2).domain());
	Line_chart.select(".line-power").attr("d", line);
	focus.select(".axis--x").call(xAxis);
	context.select(".brush")
	    .call(brush.move, x.range().map(t.invertX, t, event));
    }

    if (which == "main") {
	var doubleclickTimer = null;

	zoomer.on("click", function (event) {
	    if (doubleclickTimer) {
		window.clearTimeout(doubleclickTimer);
		doubleclickTimer = null;
		return;
	    }
	    doubleclickTimer =
		window.setTimeout(function() {
		    doubleclickTimer = null;
		    DrawSubGraph(event);
		}, 300);
	});

	/*
	 * Draw zoomed graph in lower panel, after user clicks on a point.
	 */
	function DrawSubGraph(event)
	{
	    var bins = data;
	    var x0   = x.invert(d3.pointer(event)[0]);
	    var i    = bisector(bins, x0, 1);
	    var d    = bins[i];
	    var freq = d.frequency;
	    var subdata = [];
	    var index   = (i < 25 ? 0 : i - 25);

	    for (i = index; i < index + 50; i++) {
		// Hmm, the CSV file appears to not be well sorted within
		// a frequency bin. Must be a string sort someplace.
		var sorted = bins[i].samples
		    .sort(function (a, b) { return a.frequency - b.frequency});
		subdata = subdata.concat(sorted);
	    }
	    // Clear old graph
	    d3.select(".frequency-graph-subgraph svg").remove();
	    createGraph("sub", proxy, d3.select(".frequency-graph-subgraph"), subdata);
	}
    }
}

function CreateBins(data)
{
    var result = [];
    var bins   = [];
    var hasAboveFloor= (_.has(data[0], "abovefloor") ? true : false);
    //console.info("CreateBins: ", data);

    _.each(data, function (d, index) {
	var freq  = +d.frequency;
	var power = +d.power;
	var x     = Math.floor(freq);

	if (!_.has(bins, x)) {
	    var bin = {
		"frequency" : x,
		"max"       : power,
		"min"       : power,
		"avg"       : power,
		"samples"   : [d],
	    };
	    if (hasAboveFloor) {
		bin["abovefloor"] = d.abovefloor;
		bin["violation"]  = d.violation;
		if (d.abovefloor) {
		    console.info(bin);
		}
	    }
	    bins[x] = bin;
	    result.push(bin);
	    return;
	}
	var bin = bins[x];
	if (power > bin.max) {
	    bin.max = power;
	}
	if (power < bin.min) {
	    bin.min = power;
	}
	if (hasAboveFloor) {
	    if (d.abovefloor > bin.abovefloor) {
		bin.abovefloor = d.abovefloor;
	    }
	    if (d.violation) {
		bin.violation = 1;
	    }
	    if (d.abovefloor) {
		console.info(bin);
	    }
	}
	bin.samples.push(d);
	var sum = 0;
	_.each(bin.samples, function (d) {
	    sum  += d.power;
	});
	bin.avg  = sum / _.size(bin.samples);
    });
    console.info("bins", result);
    return result;
}

function type(d) {
    d.frequency = +d.frequency;
    d.power     = +d.power;
    if (_.has(d, "center_freq")) {
	d.center_freq = +d.center_freq;
    }
    if (_.has(d, "incident")) {
	d.incident = +d.incident;
    }
    if (_.has(d, "abovefloor") && d.abovefloor != "") {
	d.abovefloor = +d.abovefloor;
    }
    else {
	d.abovefloor = 0;
    }
    if (_.has(d, "violation") && d.abovefloor != "") {
	d.violation = +d.violation;
    }
    else {
	d.violation = 0;
    }
    return d;
}

// Needed for the floating popovers, see below.
// See https://popper.js.org/docs/v2/virtual-elements/ for an
// explaination of this stuff. 
var generateGetBoundingClientRect =
    function(x1 = 0, y1 = 0, x2 = 0, y2 = 0) {
	return () => ({
	    x: x1,
	    y: y1,
	    width: 10,
	    height: 10,
	    left: x1 - 5,
	    top: y1 - 5,
	    right: x1 + 5,
	    bottom: y1 + 5,
	});
    };

export default createFrequencyGraph;
