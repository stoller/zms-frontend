import { defineStore } from 'pinia'

export const adminStore = defineStore('admin', {
  state: () => ({
    users: {
      users: [],
      page: 1,
      pages: 1,
      total: 0
    },
    elements: {
      elements: [],
      page: 1,
      pages: 1,
      total: 0
    },
    all_elements: {
      elements: [],
      page: 1,
      pages: 1,
      total: 0
    },
    all_elements_by_id: {},
    all_spectrum: {
      spectrum: [],
      page: 1,
      pages: 1,
      total: 0
    },
    all_spectrum_by_id: {},
    allocations: {
      allocations: [],
      page: 1,
      pages: 1,
      total: 0
    },
    requests: {
      requests: [],
      page: 1,
      pages: 1,
      total: 0
    },
    services: {
      services: [],
      page: 1,
      pages: 1,
      total: 0
    },
    spectrum: {
      spectrum: [],
      page: 1,
      pages: 1,
      total: 0
    },
    grants: {
      grants: [],
      page: 1,
      pages: 1,
      total: 0
    },
    claims: {
      claims: [],
      page: 1,
      pages: 1,
      total: 0
    },
    radios: {
      radios: [],
      page: 1,
      pages: 1,
      total: 0
    },
    antennas: {
      antennas: [],
      page: 1,
      pages: 1,
      total: 0
    },
    monitors: {
      monitors: [],
      page: 1,
      pages: 1,
      total: 0
    },
    observations: {
      observations: [],
      page: 1,
      pages: 1,
      total: 0
    },
    observation: {
      observation: null
    }
  }),
  actions: {
    logout() {
      this.$reset()
    },
    adminOff() {
      this.$reset()
    },
    async initAdmin() {
      if (this.all_elements.total === 'undefined'
          || this.all_elements.total !== 0) {
        console.log("skipping initAdmin; already bootstrapped all_elements list", this.all_elements.elements.length)
        return
      }

      console.log("initUser")
      this.fetchAllElements()
      this.fetchAllSpectrum()
    },
    async fetchUsers(params) {
      let response = await useNuxtApp().$usersEndpoint.list({
        ...params
      })
      if (typeof response !== 'undefined') {
        this.users = response
        console.log('fetched users (admin)', this.users.users.length)
      }
    },
    async fetchElements(params) {
      let response = await useNuxtApp().$elementsEndpoint.list({
        ...params
      })
      if (typeof response !== 'undefined') {
        this.elements = response
        console.log('fetched elements (admin)', this.elements.elements.length)
      }
    },
    async fetchServices(params) {
      let response = await useNuxtApp().$servicesEndpoint.list({
        ...params
      })
      if (typeof response !== 'undefined') {
        this.services = response
        console.log('fetched services (admin)', this.services.services.length)
      }
    },
    async fetchSpectrum(params) {
      let response = await useNuxtApp().$spectrumEndpoint.list({
        ...params
      })
      if (typeof response !== 'undefined') {
        this.spectrum = response
        console.log('fetched spectrum (admin)', this.spectrum.spectrum.length)
      }
    },
    async fetchGrants(params) {
      let response = await useNuxtApp().$grantsEndpoint.list({
        ...params
      })
      if (typeof response !== 'undefined') {
        this.grants = response
        console.log('fetched grants (admin)', this.grants.grants.length)
      }
    },
    async fetchClaims(params) {
      let response = await useNuxtApp().$claimsEndpoint.list({
        ...params
      })
      if (typeof response !== 'undefined') {
        this.claims = response
        console.log('fetched claims (admin)', this.claims.claims.length)
      }
    },
    async fetchRadios(params) {
      let response = await useNuxtApp().$radiosEndpoint.list({
        ...params
      })
      if (typeof response !== 'undefined') {
        this.radios = response
        console.log('fetched radios (admin)', this.radios.radios.length)
      }
    },
    async fetchAntennas(params) {
      let response = await useNuxtApp().$antennasEndpoint.list({
        ...params
      })
      if (typeof response !== 'undefined') {
        this.antennas = response
        console.log('fetched antennas (admin)', this.antennas.antennas.length)
      }
    },
    async fetchMonitors(params) {
      let response = await useNuxtApp().$monitorsEndpoint.list({
        ...params
      })
      if (typeof response !== 'undefined') {
        this.monitors = response
        console.log('fetched monitors (admin)', this.monitors.monitors.length)
      }
    },
    async fetchObservations(params) {
      let response = await useNuxtApp().$observationsEndpoint.list({
        ...params
      })
	if (typeof response !== 'undefined') {
            console.log("f", response);
        this.observations = response
        console.log('fetched observations (admin)', this.observations.observations.length)
      }
    },
    async fetchObservation(id, params) {
      let response = await useNuxtApp().$observationsEndpoint.read(id, {
        ...params
      })
      if (typeof response !== 'undefined') {
        this.observation = { "observation" : response };
        console.log('fetched observation (admin)', id, this.observation)
      }
    },
    async fetchAllElements(params) {
      let response = await useNuxtApp().$elementsEndpoint.list({
        page: 1,
        items_per_page: 65536
      })
      if (typeof response !== 'undefined') {
        this.all_elements = response
        console.log('fetched all_elements (admin)',
                    this.all_elements.elements.length)
        this.all_elements_by_id = {}
        for (var i = 0; i < this.all_elements.elements.length; ++i) {
          let e = this.all_elements.elements[i]
          this.all_elements_by_id[e.id] = e
        }
      }
    },
    getElementNameById(id) {
      if (this.all_elements_by_id[id] !== undefined)
        return this.all_elements_by_id[id].name
      return ""
    },
    async fetchAllSpectrum(params) {
      let response = await useNuxtApp().$spectrumEndpoint.list({
        page: 1,
        items_per_page: 65536
      })
      if (typeof response !== 'undefined') {
        this.all_spectrum = response
        console.log('fetched all_spectrum (admin)',
                    this.all_spectrum.spectrum.length)
        this.all_spectrum_by_id = {}
        for (var i = 0; i < this.all_spectrum.spectrum.length; ++i) {
          let e = this.all_spectrum.spectrum[i]
          this.all_spectrum_by_id[e.id] = e
        }
      }
    },
    getSpectrumNameById(id) {
      if (this.all_spectrum_by_id[id] !== undefined)
        return this.all_spectrum_by_id[id].name
      return ""
    }
  }
})
