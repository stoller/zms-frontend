
import { defineStore } from 'pinia'

export const userStore = defineStore('user', {
  state: () => ({
    user_id: null,
    user: {},
    can_admin: false,
    admin_role_binding: null,
    token: null,
    admin_token: null,
    element_id: null,
    elements: [],
    elements_by_id: {},
    roles: {
      roles: []
    },
    roles_by_id: {},
    roles_by_name: {},
  }),
  persist: {
    storage: persistedState.cookiesWithOptions({
      sameSite: 'strict',
      watch: true,
    }),
  },
  actions: {
    async createAdminToken() {
      return
    },
    async logout() {
      this.deleteAdminToken()
      this.deleteToken()
      this.$reset()
    },
    async deleteToken() {
      if (this.token != null && this.token.token != null) {
        console.log("deleting token", this.token.token)
        return useNuxtApp().$tokensEndpoint
          .delete(this.token.id, null)
          .then(response => {
            this.user_id = null
            this.user = {}
            this.can_admin = false
            this.element_id = null
          }).catch({})
      }
    },
    async deleteAdminToken() {
      if (this.admin_token != null && this.admin_token.token != null) {
        console.log("deleting admin token", this.admin_token.token)
        return useNuxtApp().$tokensEndpoint
          .delete(this.admin_token.id, null)
          .then(response => {
            this.admin_token = null
          }).catch({})
      }
    },
    async fetchUser() {
      console.log("fetchUser start")
      let response = await useNuxtApp().$usersEndpoint.read(this.user_id)
      if (typeof response !== 'undefined') {
        this.user = response
        console.log("fetchUser response", response)
      }
      else {
        console.log("fetchUser error", response)
      }
    },
    async fetchUserElements() {
      let response = await useNuxtApp().$usersEndpoint.readSecondary(this.user_id, "elements")
      if (typeof response !== 'undefined') {
        this.elements = response.elements
        this.elements_by_id = {}
        for (var i = 0; i < this.elements.length; ++i) {
          this.elements_by_id[this.elements[i].id] = this.elements[i]
        }
        if (this.elements.length > 0) {
          this.element_id = this.elements[0].id
        }
        console.log("fetchUserElements response", response)
      }
      else {
        console.log("fetchUserElements error", response)
      }
    },
    getElementNameById(id) {
      if (this.elements_by_id[id] !== undefined)
        return this.elements_by_id[id].name
      return ""
    },
    async fetchRoles() {
      console.log("fetchRoles start")
      let response = await useNuxtApp().$rolesEndpoint.list()
      if (typeof response !== 'undefined') {
        this.roles = response
        console.log("fetchRoles response", response)
        this.roles_by_name = {}
        for (var i = 0; i < this.roles.roles.length; ++i) {
          this.roles_by_name[this.roles.roles[i].name] = this.roles.roles[i]
        }
        this.roles_by_id = {}
        for (var i = 0; i < this.roles.roles.length; ++i) {
          this.roles_by_id[this.roles.roles[i].id] = this.roles.roles[i]
        }
      }
      else {
        console.log("fetchRoles error", response)
      }
    },
    getRoleById(id) {
      if (this.roles_by_id[id] === undefined)
        return {}
      return this.roles_by_id[id]
    },
    getRoleByName(name) {
      if (this.roles_by_name[name] === undefined)
        return {}
      return this.roles_by_name[name]
    },
    getRoleNameById(id) {
      if (this.roles_by_id[id] === undefined)
        return ""
      return this.roles_by_id[id].name
    },
    getRoleColorById(id) {
      let name = this.getRoleNameById(id)
      if (name === "admin")
        return "red"
      else if (name === "owner")
        return "deep-orange"
      else if (name === "manager")
        return "orange"
      else if (name === "operator")
        return "yellow"
      else if (name === "member")
        return "green"
      else
        return "gray"
    },
    async initUser() {
      if (typeof this.user.id !== 'undefined' && this.user.id !== null) {
        console.log("skipping initUser; have user.id", this.user.id)
        return
      }

      console.log("initUser")
      // If the token is an admin token, and we don't have an admin token
      // yet, swap and get a regular, unprivileged token.  This is necessary
      // to support red-dot (admin) mode, since an admin token cannot be
      // created from a non-admin token.
      if (this.token !== null && this.admin_token === null) {
        this.can_admin = false
        for (var i = 0; i < this.token.role_bindings.length; ++i) {
          if (this.token.role_bindings[i].role_binding.role.name === 'admin') {
            this.can_admin = true
            break
          }
        }
        if (this.can_admin) {
          this.admin_token = this.token
          
          let response = await useNuxtApp().$tokensEndpoint.create({
            method: "token",
            credential: {
              token: this.admin_token.token
            },
            admin_if_bound: false
          })
          if (typeof response !== 'undefined') {
            console.log("non-admin token", response)
            this.token = response
          }
          else {
            console.log("failed to fetch non-admin token", response)
          }
        }
      }

      this.fetchUser()
      this.fetchUserElements()
      this.fetchRoles()
    }
  },
})
