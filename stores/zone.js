
import { defineStore } from 'pinia'

export const zoneStore = defineStore('zone', {
  state: () => ({
    user: {},
    radio: {},
  }),
  actions: {
    async fetchUser(id) {
      console.log("zone.fetchUser start")
      let response = await useNuxtApp().$usersEndpoint.read(id)
      if (typeof response !== 'undefined') {
        this.user = response
        console.log("zone.fetchUser response", response)
      }
      else {
        console.log("zone.fetchUser error", response)
      }
    },
    async fetchRadio(id, params = null) {
      let response = await useNuxtApp().$radiosEndpoint.read(id, params)
      if (typeof response !== 'undefined') {
        this.radio = response
        console.log('zone.fetchRadio response', response)
      }
      else {
        console.log('zone.fetchRadio error', response)
      }
    },
  },
})
